@@
identifier f,a;
@@

int __attribute__((always_inline)) f
- (int a)
+ ()
{...}

@@
identifier f,a;
@@

int __attribute__((always_inline)) f
- (int a)
+ ()
;
